class CreateEspecializacaos < ActiveRecord::Migration[5.2]
  def change
    create_table :especializacaos do |t|
      t.string :area

      t.timestamps
    end
  end
end
