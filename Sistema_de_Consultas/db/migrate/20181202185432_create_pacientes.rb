class CreatePacientes < ActiveRecord::Migration[5.2]
  def change
    create_table :pacientes do |t|
      t.string :nome
      t.string :sexo
      t.integer :idade
      t.string :telefone
      t.string :endereco

      t.timestamps
    end
  end
end
