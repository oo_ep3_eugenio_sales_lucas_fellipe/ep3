class CreateConsulta < ActiveRecord::Migration[5.2]
  def change
    create_table :consulta do |t|
      t.time :horario
      t.date :data
      t.references :medico, foreign_key: true
      t.references :paciente, foreign_key: true
      t.string :local

      t.timestamps
    end
  end
end
