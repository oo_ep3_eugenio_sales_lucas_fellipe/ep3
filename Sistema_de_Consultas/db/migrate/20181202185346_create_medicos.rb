class CreateMedicos < ActiveRecord::Migration[5.2]
  def change
    create_table :medicos do |t|
      t.string :nome
      t.string :crm
      t.references :especializacao, foreign_key: true

      t.timestamps
    end
  end
end
