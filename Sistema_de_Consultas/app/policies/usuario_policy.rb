class UsuarioPolicy < ApplicationPolicy

	def index?
		usuario.Administrador?
	end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
