json.extract! consultum, :id, :horario, :data, :medico_id, :paciente_id, :local, :created_at, :updated_at
json.url consultum_url(consultum, format: :json)
