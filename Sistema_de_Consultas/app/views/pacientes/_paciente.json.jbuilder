json.extract! paciente, :id, :nome, :sexo, :idade, :telefone, :endereco, :created_at, :updated_at
json.url paciente_url(paciente, format: :json)
